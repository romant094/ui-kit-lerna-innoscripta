# Bundling
| [← Previous](./npm-scripts.md) | [Next →](./maintainers.md)|
| --- | --- |
In this project two bundlers are used: Webpack and Rollup.

## Webpack

Webpack builds the storybook for development and production.  
Config can be found here: `<rootDir>/.storybook/main.js`. Find key `webpackFinal`.

## Rollup

Rollup builds every package for development and production.
Config can be found here: `<rootDir>/packages/builder/src/builder.js`.
