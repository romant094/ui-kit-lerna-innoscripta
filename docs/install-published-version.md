## ⚡ How-to install published library to your project ⚡

### Steps
1. **Generate your gitlab access token**
  - Go to https://git.innoscripta.com/-/profile/personal_access_tokens
  - Choose **read_api** variant and click on **Create personal access token**
  - Make sure you copied and saved the generated token!
2. **Configure the npm**
  - Open terminal and type the command
  `npm config set @innoscripta:registry https://git.innoscripta.com/api/v4/projects/352/packages/npm/`
  - Then type another command and pass the **access token** from the first step
  `npm config set -- '//git.innoscripta.com/api/v4/projects/352/packages/npm/:_authToken' "access_token"`
3. **Install the library**
  `npm i @innoscripta/ui-kit`
4. **Rock!** 🚀🚀🚀