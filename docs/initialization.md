# Project initialization

| [← Previous](./project-structure/project-structure.md) | [Next →](./npm-scripts.md)|
| --- | --- |

## Using Docker

First, you need to install and launch [Docker](https://www.docker.com/get-started). From project root run command:

```docker
cd <project-root-dir>
docker-compose up -d --build
```

## Without Docker

For better experience you will need lerna installed globally.

```npm
npm i lerna -g
yarn global add lerna
```

To install dependencies use lerna tool. This command will install dependencies common for the whole project and related
to each package. Also, it symlinks the packages.

```npm
lerna bootstrap
```

To start the project use

```npm
npm run start-storybook
yarn start-storybook
```
