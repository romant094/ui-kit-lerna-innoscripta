# npm scripts

| [← Previous](./initialization.md) | [Next →](./bundling.md)|
| --- | --- |

## Available scripts

```json
{
  "bootstrap": "lerna bootstrap",
  "publish": "npm run build:prod && lerna publish --yes --conventional-commits",
  "start-storybook": "start-storybook -p 6006",
  "build:dev": "mode=dev lerna run build",
  "build:prod": "mode=prod lerna run build",
  "build-storybook": "build-storybook",
  "test": "jest -u",
  "test:watch": "jest -u --watch",
  "test:coverage": "jest -u --coverage",
  "lint:fix": "eslint --ext .js,.jsx,.ts,.tsx packages --color --fix",
  "prettier:format": "prettier --write 'packages/**/*.{ts,tsx,scss,css,json}'",
  "postinstall": "sh ./unlock.sh && husky install",
  "lerna:clean": "rm -rf ./node_modules && lerna clean --yes"
}
```

## Scripts description

### bootstrap

Installs dependencies common for the whole project and related to each package. Also, it symlinks the packages.

### publish

Builds all packages for production (minified, no sourcemap) and publishes them to
the [GitLab registry](https://git.innoscripta.com/libraries/frontend/npm-registry/-/packages).

### start-storybook

Starts Storybook development server on port 6006.

### build:dev

Builds all packages in development mode. The build includes two bundles (es5 and es6 standards), extracted styles,
sourcemaps, bundle analyzer. The code is not minified. Destination folder: `dist` in package folder.

### build:prod

Builds all packages in production mode. The build includes only two bundles (es5 and es6 standards). The code is
minified. Destination folder: `<dist>` in package folder.

### build-storybook

Builds storybook in production mode.

### test

Runs all tests in the project updating changed snapshots.

### test:watch

Runs all tests in the project updating changed snapshots in a watch mode. This means that the tests will run each time
when any related file is changed.

### test:coverage

Runs all tests in the project updating changed snapshots in a coverage mode. This is the same as `test` + `<coverage>`
folder is created in the project root. Also, the statistics of tests coverage will be shown in the console.

### lint:fix

Runs linter on all files and does automatic fixes if possible.

### prettier:format

Runs prettier on all files and automatically fixes the formatting.

### postinstall

This script runs after each dependency installation (e.g. npm install). In this project [npm run bootstrap](#bootstrap).

### lerna:clean

Cleans the project: removes `<node_modules>` folder in the project root and all packages root.
