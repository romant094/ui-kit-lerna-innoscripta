# UI-Kit

## Installation

[Guide to install published version to your project](./docs/install-published-version.md)

### Using ssh

```git
git clone git@git.innoscripta.com:libraries/frontend/ui-kit.git
```

### Using https

```git
git clone https://git.innoscripta.com/libraries/frontend/ui-kit.git
```

## Documentation

- [Project structure](./docs/project-structure/project-structure.md)
- [Project initialization](./docs/initialization.md)
- [npm scripts](./docs/npm-scripts.md)
- [Bundling](./docs/bundling.md)
- [How to create a new component](./docs/creating-new-component.md)
- [How to publish packages](./docs/publishing.md)
- [Maintainers](./docs/maintainers.md)
