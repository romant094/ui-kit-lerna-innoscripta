export default {
  clearMocks: true,
  collectCoverage: true,
  collectCoverageFrom: [
    'packages/**/*.{[tj]s?(x)}',
    '!packages/builder/**',
    '!packages/ui-kit/**',
    '!**/node_modules/**',
    '!**/vendor/**'
  ],
  coverageDirectory: 'coverage',
  setupFilesAfterEnv: ['<rootDir>/enzyme.setup.js'],
  testEnvironment: 'jsdom',
  unmockedModulePathPatterns: ['node_modules/react/', 'node_modules/enzyme/'],
  verbose: true,
  moduleNameMapper: {
    '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
      '<rootDir>/__mocks__/fileMock.js',
    '\\.(css|scss)$': 'identity-obj-proxy'
  }
}
