import React from 'react'
import cx from 'clsx'
import styles from './styles.module.css'

export type TextProps = {
  children?: React.ReactNode
  className?: string
  variant?: string
  as?: string
}

export const Text: React.FC<TextProps> = ({
  children,
  className,
  as = 'p',
  variant = '',
  ...rest
}) => {
  const textVariant = styles[variant] || 'body'
  const classes = cx(styles.text, { [textVariant]: variant }, className)
  return React.createElement(
    as,
    {
      ...rest,
      className: classes
    },
    children
  )
}
