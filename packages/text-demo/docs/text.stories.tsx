import React from 'react'
import { Text, TextProps } from '../src'
import { Story, Meta } from '@storybook/react'

export default { title: 'Text', component: Text } as Meta
const Template: Story<TextProps> = args => <Text {...args} />

export const Body = Template.bind({})
Body.args = {
  children: 'Body Text'
}

export const Hero = Template.bind({})
Hero.args = {
  children: 'Hero Text',
  variant: 'hero'
}

export const Heading = Template.bind({})
Heading.args = {
  children: 'Heading Text',
  variant: 'heading'
}
