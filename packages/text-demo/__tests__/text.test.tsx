import React from 'react'
import { render } from 'enzyme'
import { Text } from '../src'

const mockContent = 'Dummy text'
const wrapper = render(<Text>{mockContent}</Text>)

describe('Text component', () => {
  it('should render', () => {
    expect(wrapper).toMatchSnapshot()
  })
})
