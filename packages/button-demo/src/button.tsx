import React from 'react'
import cx from 'clsx'
import styles from './styles.module.css'

export type ButtonProps = {
  label: React.ReactNode
  className?: string
  variant?: string
  onClick?: () => void
}

export const Button: React.FC<ButtonProps> = ({
  label,
  className,
  variant,
  ...rest
}) => {
  const classes = cx(
    styles.button,
    { [styles.buttonSecondary]: variant === 'secondary' },
    className
  )
  return (
    <button {...rest} className={classes}>
      {label}
    </button>
  )
}
