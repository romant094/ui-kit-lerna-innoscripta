import React from 'react'
import type { Story, Meta } from '@storybook/react'
import { Button, ButtonProps } from '../src'

export default { title: 'Button', component: Button } as Meta
const Template: Story<ButtonProps> = args => <Button {...args} />

export const Primary = Template.bind({})
Primary.args = { variant: 'primary', label: 'Button' }

export const Secondary = Template.bind({})
Secondary.args = { ...Primary.args, variant: 'secondary', label: '😄👍😍💯' }

export const Tertiary = Template.bind({})
Tertiary.args = { ...Primary.args, label: '📚📕📈🤓' }
