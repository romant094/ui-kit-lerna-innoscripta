import React from 'react'
import { render } from 'enzyme'
import { Button } from '../src'

const mockContent = 'Dummy button'
const wrapper = render(<Button>{mockContent}</Button>)

describe('Button component', () => {
  it('should render', () => {
    expect(wrapper).toMatchSnapshot()
  })
})
