FROM node:16.10-alpine

WORKDIR /app

COPY . .

RUN npm i lerna -g --silent
RUN lerna bootstrap
RUN npm run build-storybook

EXPOSE 6006

CMD ["npm", "run", "storybook"]
