module.exports = {
  core: {
    builder: 'webpack5'
  },
  stories: ['../packages/**/*.stories.@(jsx|tsx)'],
  addons: [
    '@storybook/addon-links',
    '@storybook/addon-essentials',
    '@storybook/addon-a11y',
    'storybook-css-modules-preset',
    {
      name: '@storybook/addon-postcss',
      options: {
        postcssLoaderOptions: {
          implementation: require('postcss'),
        }
      }
    }
  ],
  webpackFinal: async config => {
    // config.resolve.mainFields = ['browser', 'src', 'module', 'main']
    return config
  }
}
